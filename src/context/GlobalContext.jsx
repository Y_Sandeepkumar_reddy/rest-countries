
import React, { useContext,useState } from "react";
import { createContext } from "react";

const GlobalContext = React.createContext()

export function useGlobalContext(){
    return useContext(GlobalContext)
}

export function GlobalProvider({children}){

    const [regionalDropDown, setRegionalDropDown] = useState(false)
    const [subRegionalDropDown, setSubRegionalDropDown] = useState(false)
    const [sortByPopulationDropDown, setSortByPopulationDropDown] = useState(false)
    const [sortByAreaDropDown, setSortByAreaDropDown] = useState(false)
    const [darkMode, setDarkMode] = useState(false)
    const [countryData, setCountryData] = useState([])
    const [searchInput, setSearchInput] = useState('')
    const [selectedRegion, setSelectedRegion] = useState('All')
    const [selectedSubRegion, setSelectedSubRegion]= useState('All')
    const [selectedSortByPopulation, setSelectedSortByPopulation] = useState()
    const [selectedSortByArea, setSelectedSortByArea] = useState()
    
    const value= {
        regionalDropDown,
        setRegionalDropDown,
        countryData,
        setCountryData,
        selectedRegion,
        setSelectedRegion,
        searchInput,
        setSearchInput,
        darkMode,
        setDarkMode,
        selectedSubRegion,
        setSelectedSubRegion,
        subRegionalDropDown,
        setSubRegionalDropDown,
        sortByPopulationDropDown,
        setSortByPopulationDropDown,
        selectedSortByPopulation,
        setSelectedSortByPopulation,
        sortByAreaDropDown,
        setSortByAreaDropDown,
        selectedSortByArea,
        setSelectedSortByArea
    }

    return <GlobalContext.Provider value={value}>{children}</GlobalContext.Provider>
}