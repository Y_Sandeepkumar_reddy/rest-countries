import React from 'react'
import './RegionalDropDown.css'
import { useGlobalContext } from '../../context/GlobalContext'

export default function DropDown() {

  const {selectedRegion, setSelectedRegion, setRegionalDropDown, regionalDropDown, setSearchInput, darkMode, setDarkMode, setSelectedSubRegion} = useGlobalContext()

  const changeRegionHandler = (event) => {
    if(event.target.tagName == 'SPAN'){
      
      setSelectedRegion(event.target.id)
      setRegionalDropDown(false)
      setSearchInput('')
      setSelectedSubRegion('All')
    }
  }

  const lightStyle = {
    backgroundColor: 'white',
    color: 'black'
  }
  const darkStyle = {
    backgroundColor: 'rgb(43, 57, 69)',
    color: 'white',
    boxShadow: "0 0 20px rgba(0, 0, 0, 0.5)" ,
  }
    
  return (
    <div className='dropdown' onClick={changeRegionHandler} style={darkMode? darkStyle : lightStyle}>
        <span id='All' className={`dropdown-option ${selectedRegion=='All' ? "selected" : ""}`}>All</span>
        <span id="Africa" className={`dropdown-option ${selectedRegion=='Africa' ? "selected" : ""}`}>Africa</span> 
        <span id="America" className={`dropdown-option ${selectedRegion=='America' ? "selected" : ""}`}>America</span>
        <span id="Asia" className={`dropdown-option ${selectedRegion=='Asia' ? "selected" : ""}`}>Asia</span>
        <span id="Europe" className={`dropdown-option ${selectedRegion=='Europe' ? "selected" : ""}`}>Europe</span>
        <span id="Oceania" className={`dropdown-option ${selectedRegion=='Oceania' ? "selected" : ""}`}>Oceania</span>
    </div>
  )
}
