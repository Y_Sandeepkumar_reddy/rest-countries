import React from 'react'
import './Card.css'
import { useGlobalContext } from '../../context/GlobalContext';

export default function Card(information) {

    const {name, population, region, capital, flag_url} = information;
    const {darkMode, setDarkMode} = useGlobalContext()

    const cardStyleDark = {
      backgroundColor: 'hsl(209, 23%, 22%)',
      boxShadow: "0 0 20px rgba(0, 0, 0, 0.5)" ,
      color: 'white'
    }
    const cardStyleLight = {
      backgroundColor: 'white',
      color: 'black'
    }

  return (
    <ul className='card' style={ !darkMode ? cardStyleLight : cardStyleDark} >
        <img src={flag_url}/>
        <li className='country_name'>{name}</li>
        <li className='population'><b>Population</b>: {population}</li>
        <li className='region'><b>Region</b>: {region}</li>
        <li className='capital'><b>Capital</b>: {capital}</li>
    </ul>
  )
}
