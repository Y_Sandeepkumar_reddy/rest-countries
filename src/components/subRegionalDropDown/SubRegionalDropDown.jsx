import React from 'react'
import './SubRegionalDropDown.css'
import { useGlobalContext } from '../../context/GlobalContext';

export default function SubRegionalDropDown() {

    const {selectedSubRegion, subRegionalDropDown, setSubRegionalDropDown, setSelectedSubRegion , setSearchInput, darkMode, selectedRegion} = useGlobalContext() 
    const subRegions = { 
        "All":[
            "All",
            "Australia and New Zealand",
            "Caribbean",
            "Central America",
            "Central Asia",
            "Central Europe",
            "Eastern Africa",
            "Eastern Asia",
            "Eastern Europe",
            "Melanesia",
            "Micronesia",
            "Middle Africa",
            "North America",
            "Northern Africa",
            "Northern Europe",
            "Polynesia",
            "South America",
            "South-Eastern Asia",
            "Southeast Europe",
            "Southern Africa",
            "Southern Asia",
            "Southern Europe",
            "Western Africa",
            "Western Asia",
            "Western Europe",
        ],
        "Africa": [
            "All",
            "Eastern Africa",
            "Middle Africa",
            "Northern Africa",
            "Southern Africa",
            "Western Africa"
        ],
        "America": [
            "All",
            "Caribbean",
            "Central America",
            "North America",
            "South America"
        ],
        "Asia": [
            "All",
            "Central Asia",
            "Eastern Asia",
            "Southern Asia",
            "Southeast Asia",
            "Western Asia"
        ],
        "Europe": [
            "All",
            "Central Europe",
            "Eastern Europe",
            "Northern Europe",
            "Southern Europe",
            "Southeast Europe",
            "Western Europe"
        ],
        "Oceania": [
            "All",
            "Australia and New Zealand",
            "Melanesia",
            "Micronesia",
            "Polynesia"
        ]

    };

    const changeSubRegionHandler = (event) => {
        if(event.target.tagName == 'SPAN'){
      
            setSelectedSubRegion(event.target.id)
            setSubRegionalDropDown(false)
            setSearchInput('')
        }
    }

    const lightStyle = {
        backgroundColor: 'white',
        color: 'black'
    }
    const darkStyle = {
        backgroundColor: 'rgb(43, 57, 69)',
        color: 'white',
        boxShadow: "0 0 20px rgba(0, 0, 0, 0.5)" ,
    }
        

  return (
    <div className='dropdown subregional' onClick={changeSubRegionHandler} style={darkMode? darkStyle : lightStyle} >
        {
            subRegions[selectedRegion].map((subRegion) => {
                return <span id={subRegion}  className={`dropdown-option ${selectedSubRegion==subRegion ? "selected" : ""}`}>{subRegion}</span> 
            })
        }
    </div>
  )
}
