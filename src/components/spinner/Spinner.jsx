import React from 'react'
import './Spinner.css'

export default function Spinner() {
  return (
    <div className='spinner'>
        <div class="spinner-border text-primary" role="status">
            <span class="sr-only"> Loading...</span>
        </div>
        <span>&nbsp;  Loading ...</span>
    </div>
  )
}
