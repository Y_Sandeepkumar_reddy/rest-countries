import React from 'react'
import './Error.css'
import { useGlobalContext } from '../../context/GlobalContext'

export default function Error({error}) {
    const {selectedRegion, setSelectedRegion} = useGlobalContext()
    
  return (
    <div>
        <div className='error_container'>
            <p>{error}</p>
        </div>
    </div>
  )
}
