import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import Header from './components/header/Header'
import Home from './components/home/Home'
import { useGlobalContext } from './context/GlobalContext'

function App() {
  
  const {darkMode, setDarkMode} = useGlobalContext()

  const lightStyle= {
    backgroundColor: 'white',
    color: 'black'

  }
  const darkStyle= {
    backgroundColor: 'rgb(32, 44, 55)',
    color: 'white'
  }

 
  return (
    <div id="container" style={!darkMode? lightStyle : darkStyle}>
      <Header />
      <Home   />
    </div>
  )
}

export default App
